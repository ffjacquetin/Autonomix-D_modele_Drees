# Copyright (C) 2021. Logiciel élaboré par l'État, via la Drees.

# Pole Simulation du bureau Handicap et Dépendance, Drees.

# Ce programme informatique a été développé par la Drees. Il permet d'exécuter le modèle de microsimulation Autonomix version 1 de décembre 2021.

# Ce programme a été exécuté le 10/12/2021 avec la version 4.0.5 de R et les packages mobilisés sont listés dans le fichier DESCRIPTION du projet »

# La documentation du modèle peut être consultése sur gitlab : https://drees_code.gitlab.io/OSOL/bhd/autonomix-documentation-externe/

# Ce programme utilise les données de la base CARE Ménage 2015, ou des RI 2017 ou des RI 2017 anonymisées dans leur version du <date de la version utilisée>.

# Bien qu'il n'existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu'ils y rencontreraient, en écrivant à DREES-CODE@sante.gouv.fr

# Ce logiciel est régi par la licence EUPL V1.2 soumise au droit français et européen, respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence EUPL V1.2, telle que diffusée sur https://eupl.eu/1.2/fr/ et reproduite dans le fichier LICENCE diffusé conjointement au présent programme.
# En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.
# À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.


construit_base <- function(chemin_base_brute, chemin_base_construite, type_base) {
  #' construit_base
  #'
  #' Meta-fonction appelant la bonne sous-fonction de création de base en fonction
  #' du type de données voulu
  #'
  #' @param chemin_base_brute character, chemin vers le dossier contenant les bases (care) ou la base elle-même (RI)
  #' @param chemin_base_construite character, chemin vers le dossier où écrire la base construite
  #' @param type_base character, à choisir parmi "care", "ri", "ri_floutees"
  #' @export

  bases_ri = list(
    'ri' = "RI2017/BEN_APAD_CASD.RData",
    'ri_floutees' =  "RI2017/ben_apad_floutee_v1.RData"
    )

  if (donnees_base %in% c("ri", "ri_floutees")) {
    nom_base_ri <- bases_ri[donnees_base]
    chemin_lecture_donnees <- paste(chemin_donnees_brutes, nom_base_ri, sep = '/')
    } else {
    chemin_lecture_donnees <- chemin_donnees_brutes
    }

  chemin_constitution <- paste(chemin_base_construite, '2-Constitution_bases', nom_branche, sep = '/')

  if (!dir.exists(chemin_constitution)) {
    dir.create(chemin_constitution, recursive = TRUE)
    print(paste("Le fichier", chemin_constitution, "n'existait pas. Il a été créé."))
    }

  if (type_base == 'care') {
    constitution_base_care_casd(chemin_lecture_donnees, chemin_constitution)
    } else if (type_base == 'ri') {
    lancement_constitution_ri(chemin_lecture_donnees, chemin_constitution)
    } else if (type_base == 'ri_floutees') {
    lancement_constitution_ri_floutees(chemin_lecture_donnees, chemin_constitution)
    } else {
      print(paste("Le type de base", type_base, "n'a pas été reconnu. Veuillez choisir un type parmi 'care', 'ri', 'ri_floutees' "))
    }
  }
