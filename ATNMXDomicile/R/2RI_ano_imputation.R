# Copyright (C) 2021. Logiciel élaboré par l'État, via la Drees.

# Pole Simulation du bureau Handicap et Dépendance, Drees.

# Ce programme informatique a été développé par la Drees. Il permet d'exécuter le modèle de microsimulation Autonomix version 1 de décembre 2021.

# Ce programme a été exécuté le 10/12/2021 avec la version 4.0.5 de R et les packages mobilisés sont listés dans le fichier DESCRIPTION du projet »

# La documentation du modèle peut être consultése sur gitlab : https://drees_code.gitlab.io/OSOL/bhd/autonomix-documentation-externe/

# Ce programme utilise les données de la base CARE Ménage 2015, ou des RI 2017 ou des RI 2017 anonymisées dans leur version du <date de la version utilisée>.

# Bien qu'il n'existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu'ils y rencontreraient, en écrivant à DREES-CODE@sante.gouv.fr

# Ce logiciel est régi par la licence EUPL V1.2 soumise au droit français et européen, respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence EUPL V1.2, telle que diffusée sur https://eupl.eu/1.2/fr/ et reproduite dans le fichier LICENCE diffusé conjointement au présent programme.
# En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.
# À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.


lancement_imputations_ri_anonymises <- function(chemin_constitution_, chemin_imputation_) {
  #' lancement_imputations_ri_anonymises
  #'
  #' Procède aux imputation nécessaires pour les remontées individuelles
  #'
  #' @param chemin_constitution_ character, chemin vers les bases construites à partir des données brutes
  #' @param chemin_imputation_ character, chemin où écrire les bases issues de l'imputation
  #'
  #' @return data.frame contenant les données ri imputées
  #'
  #' @importFrom descr freq
  #' @importFrom dplyr filter mutate left_join rename select summarise
  #' @importFrom lubridate time_length interval ymd
  #' @importFrom AER tobit
  #' @importFrom stats glm gaussian predict quantile rnorm
  #' @importFrom magrittr %>%
  #' @importFrom data.table fread fwrite
  #' @importFrom survival survreg Surv
  #'
  #' @export
  #'
  #'

  dfri <- fread(paste(chemin_constitution_, "base-ano-data_ri2017_constitution_base.csv",sep='/'))

  dfri <- dfri %>% rename(SEXE_C=SEXE)
  freq(dfri$SEXE_C,useNA="always" ,plot=F)
  dfri <- dfri %>%  rename(gir_imput=GIRFIN_APA)
  freq(dfri$gir_imput,useNA="always" ,plot=F)
  dfri <- dfri %>%   mutate(AGE_C=AGER)
  summary(dfri$AGE_C,useNA="always")

  dfri <- dfri %>% mutate(IDENT_SEN = ID)

  freq(dfri$couple,useNA="always",plot=F)
  freq(dfri$couple,useNA="always",w=dfri$poids_p,plot=F)

  # avant de supprimer les couples=9, on repondère les obs.
  poids_couple_01 <- dfri %>% filter(!(couple%in%9)) %>%  summarise(n_tot = n())
  poids_couple_9 <- dfri %>% filter(couple%in%9) %>%
    summarise(n_tot = sum(poids_p))

  poids_c = poids_couple_9$n_tot/poids_couple_01$n_tot

  # affectation des poids calculés à la base des droits ouverts
  #rid_do$poids_do <- 1
  dfri$poids_p_c[dfri$couple%in%9] <- 0
  dfri$poids_p_c[!(dfri$couple%in%9)] <- dfri$poids_p[!(dfri$couple%in%9)] + poids_c

  sum(dfri$poids_p_c)
  sum(dfri$poids_p)

  dfri <- dfri %>% filter(couple!=9)
  #594907 obs
  sum(dfri$poids_p_c) # 766 905
  sum(dfri$poids_p)  # 738 576

  #766 905 individus pondérés
  dfri <- dfri %>% mutate(couple_rfs=  case_when(
    RESSOURC_APA_IND==0 & couple==0 & TYPRESS_APA==2 ~ 1,
    RESSOURC_APA_IND==0 & couple==0 & TYPRESS_APA==3 ~ 1,
    TRUE ~ as.numeric(couple)
  ))


  dfri$couple_rfs<-as.factor(dfri$couple_rfs)
  #dfri$dfri<-as.factor(dfri$etamatri_rfs)
#


  #imputation des ressources ------
  #verif répartition par tranche de ress par rapport data drees
  #sachant qu'on a repondéré
  dfri <- dfri %>% mutate(TR_RESSOURC_APA = case_when(
    RESSOURC_APA < 803 ~ "1. moins de 803",
    RESSOURC_APA >= 803 & RESSOURC_APA < 1000 ~ "2.[803-1000)",
    RESSOURC_APA >= 1000 & RESSOURC_APA < 1200 ~ "3.[1000-1200)",
    RESSOURC_APA >= 1200 & RESSOURC_APA < 1400 ~ "4.[1200-1400)",
    RESSOURC_APA >= 1400 & RESSOURC_APA < 1600 ~ "5.[1400-1600)",
    RESSOURC_APA >= 1600 & RESSOURC_APA < 1800 ~ "6.[1600-1800)",
    RESSOURC_APA >= 1800 & RESSOURC_APA < 2000 ~ "7.[1800-2000)",
    RESSOURC_APA >= 2000 & RESSOURC_APA < 2500 ~ "8.[2000-2500)",
    RESSOURC_APA >= 2500 ~ "9.plus de 2500"

  ))

  dfri <- dfri %>% mutate(age_plus60 = AGER - 60,
                          age_plus60_carre = age_plus60^2)

  test<- dfri %>% filter(is.na(RESSOURC_APA))
  freq(test$DEP,plot=F,useNA="always")
  #D002 représente 61% des manquants


  dfri <- dfri %>% mutate(RESSOURAPA_non_impute=RESSOURC_APA)
  summary(dfri$RESSOURAPA_non_impute,useNA="always")
  dfri <- dfri %>% mutate(taux_particip=case_when(
    !is.na(APA) & !is.na(NOTPB)  ~ NOTPB/APA ,
    TRUE  ~ NA_real_
  ))

  summary(dfri$taux_particip,useNA="always")
  test <- dfri %>% filter(RESSOURC_APA>=30000) # 21 obs
  freq(test$DEP,plot=F,useNA="always")# dont 8 dans le 75, cela parait juste


  #glm
  #seulement pour le valeurs>0 et < 30000
  test <- dfri %>% filter(RESSOURAPA_non_impute==0)
  #5401 obs
  travail<- dfri %>% filter(RESSOURAPA_non_impute>0 & RESSOURAPA_non_impute<30000)
  model_glm <- glm(RESSOURAPA_non_impute ~ couple_rfs + SEXE_C + age_plus60 + age_plus60_carre+DEP,
                   data = travail , na.action = "na.exclude",
                   family = gaussian(link = "log"))

  summary(model_glm)
  ressource_apa_imput_glm <- predict(model_glm, newdata = dfri,type="response")
  dfri$ress_glm <- ressource_apa_imput_glm
  dfri$RESSOURC_APA[is.na(dfri$RESSOURC_APA)] <- ressource_apa_imput_glm[is.na(dfri$RESSOURC_APA)]

  summary(dfri$RESSOURC_APA,useNA="always")
  summary(dfri$RESSOURAPA_non_impute,useNA="always")

  quantile(as.numeric(dfri$RESSOURAPA_non_impute),na.rm=TRUE,seq(0.1,1,by=0.1))
  quantile(as.numeric(dfri$RESSOURC_APA),na.rm=TRUE,seq(0.1,1,by=0.1))


  #imputation du plan-------

  #correction plan, AIDEHUM_NOT et part AH
  dfri <- dfri %>% mutate(plan_2017=APA)
  summary(dfri$plan_2017,useNA="always") #31 417 NA
  summary(dfri$AIDEHUM_NOT,useNA="always")

  test<-dfri %>% filter(plan_2017<AIDEHUM_NOT)
  #3050
  test<-test %>% mutate(diff=plan_2017-AIDEHUM_NOT)
  summary(test$diff)#de -62 à 1
  #Min. 1st Qu.  Median    Mean 3rd Qu.    Max.
  #-62.00   -1.00   -1.00   -1.18   -1.00   -0.08

  dfri <- dfri %>% mutate(AIDEHUM_NOT=case_when(
    plan_2017<AIDEHUM_NOT  ~ as.numeric(plan_2017) ,
    TRUE  ~ as.numeric(AIDEHUM_NOT)
  ))




  dfri <- dfri %>% mutate(proportion_ah_annee_base=AIDEHUM_NOT/plan_2017)
  summary(dfri$proportion_ah_annee_base,useNA="always")
  #83 871 valeurs manquantes

  summary(dfri$AIDEHUM_NOT,useNA="always")
  #dfri$AIDEHUM_NOT[dfri$AIDEHUM_NOT==999999999] <- NA
  #summary(dfri$AIDEHUM_NOT,useNA="always")
  #64 918 NA
  table(dfri$AIDEHUM)
  test <- dfri %>% filter(AIDEHUM==0)
  summary(test$proportion_ah_annee_base,useNA="always")
  #4817 qui sont manquant qu'on peut mettre à 0
  dfri$proportion_ah_annee_base[dfri$AIDEHUM==0] <- 0
  dfri$AIDEHUM_NOT[dfri$AIDEHUM==0] <- 0
  summary(dfri$proportion_ah_annee_base,useNA="always")


  freq(dfri$AIDEHUM,plot=F,useNA="always")
  freq(dfri$AUTREAIDE,plot=F,useNA="always")
  freq(dfri$AIDEPONC,plot=F,useNA="always")
  freq(dfri$AIDEHEB,plot=F,useNA="always")
  freq(dfri$AIDEACC,plot=F,useNA="always")
  freq(dfri$AIDETRANS,plot=F,useNA="always")


  test <- dfri %>% filter(AIDEHUM==1 & AUTREAIDE==0 & AIDEPONC==0)
  #237 521
  test <- test %>% filter(AIDEHEB==0 & AIDEACC==0 & AIDETRANS==0)
  #213 692
  summary(test$proportion_ah_annee_base,useNA="always")
  #24 489 NA, on peut corriger proportion_ah_annee_base = 1

  test <- dfri %>% filter(AIDEHUM==1 & AUTREAIDE==0 & AIDEPONC==0 & AIDEHEB==0 & AIDEACC==0 & AIDETRANS==0 )
  summary(test$proportion_ah_annee_base,useNA="always")
  #s'il n'y a que de l'aide hu, on corrige les variables

  dfri <- dfri %>% mutate(proportion_ah_annee_base=case_when(
    AIDEHUM==1 & AUTREAIDE==0 & AUTREAIDE_I==0 & AIDEPONC_I==0 & AIDEHEB==0 & AIDEACC==0 & AIDETRANS==0   ~ 1 ,
    TRUE  ~ proportion_ah_annee_base
  ))
  summary(dfri$proportion_ah_annee_base,useNA="always")


  test <- dfri %>% filter(AIDEHUM==1 & AUTREAIDE==0 & AIDEPONC==0 & AIDEHEB==0 & AIDEACC==0 & AIDETRANS==0)
  test1 <- test %>% filter(!is.na(AIDEHUM_NOT) & is.na(plan_2017))
  # on peut corriger le plan par AIDEHUM_NOT qd plan_2017 manquant et qu'il n'y a que de l'aide hu
  #6148 obs
  test1 <- test %>% filter(is.na(AIDEHUM_NOT) & !is.na(plan_2017))
  # on peut corriger AIDEHUM_NOT par le plan qd AIDEHUM_NOT manquante il n'y a que de l'aide hu
  #17 569 NA

  dfri <- dfri %>% mutate(plan_2017=case_when(
    AIDEHUM==1 & AUTREAIDE==0 & AIDEPONC==0 & AIDEHEB==0 & AIDEACC==0 & AIDETRANS==0
    & !is.na(AIDEHUM_NOT) & is.na(plan_2017)  ~ as.numeric(AIDEHUM_NOT) ,
    TRUE  ~ as.numeric(plan_2017)
  ))

  summary(dfri$plan_2017,useNA="always")
  #25 269 NA

  dfri <- dfri %>% mutate(AIDEHUM_NOT=case_when(
    AIDEHUM==1 & AUTREAIDE==0 & AIDEPONC==0 & AIDEHEB==0 & AIDEACC==0 & AIDETRANS==0
    & is.na(AIDEHUM_NOT) & !is.na(plan_2017)  ~ as.numeric(plan_2017) ,
    TRUE  ~ as.numeric(AIDEHUM_NOT)
  ))

  summary(dfri$AIDEHUM_NOT,useNA="always")
  #45 328 NA

  test<-  dfri %>%
    filter(is.na(plan_2017))
  freq(test$DEP,useNA="always",plot=F)


  test<-dfri %>% filter(proportion_ah_annee_base>1)
  #0
  dfri <- dfri %>% mutate(plan_2017_non_imput=as.numeric(plan_2017))

  # Calcul de la durée de l'APA en mois jusque décembre 2017
  summary(dfri$DATE_APAD)
  dfri$DATE_APAD <-ymd(dfri$DATE_APAD)
  date_3112 <- ymd("2017-12-31")

  dfri$DATE_APAD[dfri$DATE_APAD==ymd("9998-12-30")] <- NA
  dfri$DATE_APAD[dfri$DATE_APAD>date_3112] <- NA

  dfri <- dfri %>% mutate(duree_apad_m = time_length(interval(start = ymd(DATE_APAD), end = date_3112), unit = "months"))
  summary(dfri$duree_apad_m)

  dfri <- dfri %>% mutate(duree_apad_y = time_length(interval(start = ymd(DATE_APAD), end = date_3112), unit = "years"))
  summary(dfri$duree_apad_y)

  dfri <- dfri %>% mutate(duree_apad_t = case_when(
    duree_apad_m < 12 ~ "Moins d'1 an",
    12 <= duree_apad_m & duree_apad_m<24 ~ "[1-2 ans)",
    24<= duree_apad_m & duree_apad_m<36 ~ "[2-3 ans)",
    36<= duree_apad_m & duree_apad_m<48 ~  "[3-4 ans)",
    48 <= duree_apad_m & duree_apad_m<60 ~ "[4-5 ans)",
    60 <= duree_apad_m & duree_apad_m<72 ~ "[5-6 ans)",
    72 <= duree_apad_m & duree_apad_m<96 ~ "[6-8 ans)",
    96 <=duree_apad_m ~  "8 ans et +",
    is.na(duree_apad_m)  ~ "Manquant"

  ))
  freq(dfri$duree_apad_t,useNA="always",plot=F)
  test <-  dfri %>%
    filter(duree_apad_t=="Manquant")
  freq(test$DEP,useNA="always",plot=F)

  freq(dfri$gir_imput,useNA="always",plot=F)

  dfri$gir_imput <-as.integer(dfri$gir_imput)
  class(dfri$gir_imput)
  num_GIR<-1
  class(dfri$ID)

  #modele Tobit pour le plan
  dfri <- dfri %>% mutate(PLAFOND_2017=case_when(
    gir_imput==1 ~ 1714.79,
    gir_imput==2 ~ 1376.91,
    gir_imput==3 ~ 994.87,
    gir_imput==4 ~ 663.61

  ))
  sum(is.na(dfri$PLAFOND_2017))

  dfri <- dfri %>% mutate(ratio_plan_plafond = pmin(plan_2017 / PLAFOND_2017, 0.96))
  summary(dfri$ratio_plan_plafond)

  dfri1 <- dfri %>% filter(!(DEP %in% c("002","082")))
  summary(dfri1$ratio_plan_plafond)

  #dfri <- dfri %>% mutate(ID=paste0(IDENT,"_",DEP))
  # Je crée une fonction qui filtre le dataframe en ne prenant que le gir num_GIR comme parametre
  set.seed(123)
  fit_tobit_plan <- function(dfri1, num_GIR,left_cens = 0,  right_cens = 0.96){

    df_temp <- dfri1%>%
      filter (gir_imput == num_GIR)

    model <- tobit(ratio_plan_plafond ~
                     age_plus60 + SEXE_C + duree_apad_t + couple_rfs + RESSOURC_APA ,
                   dist ="gaussian",
                   left=left_cens,
                   right = right_cens,
                   data = df_temp,
                   na.action = "na.exclude")

    yhat <- predict(model, newdata = df_temp )
    yhat <- data.frame("ID" = df_temp$ID,
                       "y" = as.numeric(yhat))

    return(list(model = model, yhat = yhat))
  }

  # On met les variables continues pour augmenter le nbre de degrés de liberté
  # on applique la fonction fit_tobit au GIR1
  tob1 <- fit_tobit_plan(dfri1, 1)
  # summary(tob1[["model"]])
  # on applique la fonction fit_tobit au GIR2
  tob2 <- fit_tobit_plan(dfri1, 2)
  # summary(tob2[["model"]])
  # on applique la fonction fit_tobit au GIR3
  tob3 <- fit_tobit_plan(dfri1, 3)
  # summary(tob3[["model"]])
  # on applique la fonction fit_tobit au GIR4
  tob4 <- fit_tobit_plan(dfri1, 4)
  # summary(tob4[["model"]])

  # On groupe les prédictions issues de chaque modèle en une table avec l'identifiant et la prédiction
  fitted <- rbind(tob1[["yhat"]], tob2[["yhat"]], tob3[["yhat"]], tob4[["yhat"]])

  # On ajoute le besoin d'aide imputé à partir de la prédiction Tobit
  # pour cela on left_join la table apa_domicile_3 avec le fitted par la clef ID
  dfri_1 <- dfri1 %>% left_join(fitted, by = "ID")
  dfri_1 <- dfri_1 %>% rename(ratio_plan_plafond_imput = y)

  scale_t <- c(
    tob1[["model"]]$scale, tob2[["model"]]$scale,
    tob3[["model"]]$scale, tob4[["model"]]$scale
  )

  # sd_ratio <- 0.5
  sd_ratio <- 1
  set.seed(123)
  alea1 <- rnorm(n = nrow(dfri_1), sd = sd_ratio)
  alea2 <- rnorm(n = nrow(dfri_1), sd = sd_ratio)
  alea3 <- rnorm(n = nrow(dfri_1), sd = sd_ratio)
  alea4 <- rnorm(n = nrow(dfri_1), sd = sd_ratio)
  alea5 <- rnorm(n = nrow(dfri_1), sd = sd_ratio)

  dfri_1 <- dfri_1 %>%
    mutate(
      part_plan_alea =
        ajout_alea_plan(
          scale_t,
          ratio_plan_plafond_imput,
          1 * (gir_imput == 1) + 2 * (gir_imput == 2) +
            3 * (gir_imput == 3) + 4 * (gir_imput == 4),
          alea1, alea2, alea3, alea4, alea5
        )
    )

  dfri_1 <- dfri_1 %>%
    mutate(ratio_plan_plafond_imput = part_plan_alea)



  dfri_1 <- dfri_1 %>%
    mutate(plan_Deplaf_2017 =
             ratio_plan_plafond_imput * PLAFOND_2017)


  summary(dfri_1$plan_Deplaf_2017)
  test<- dfri_1 %>%
    filter(plan_Deplaf_2017<0)# 3 obs

  #on * par -1 les 3 valeurs négatives
  #dfri_1$plan_Deplaf_2017[dfri_1$plan_Deplaf_2017<0] <- (-1)*dfri_1$plan_Deplaf_2017[dfri_1$plan_Deplaf_2017<0]
  #on impute par la moyenne les valeurs négatives
  dfri_1$plan_Deplaf_2017[dfri_1$plan_Deplaf_2017<0] <- mean(dfri_1$plan_Deplaf_2017)

  dfri_1 <- dfri_1 %>% mutate(plan_Replaf_2017  = case_when(
    plan_Deplaf_2017 >
      PLAFOND_2017 ~ PLAFOND_2017,
    TRUE ~ plan_Deplaf_2017
  )
  )

  summary(dfri_1$plan_Deplaf_2017)
  summary(dfri_1$plan_Replaf_2017)


  #plan_Replaf_2017 : si on impute pour tous le monde
  # plan_2017 : si on impute que les manquants

  dfri_1$plan_2017[is.na(dfri_1$plan_2017_non_imput)] <- dfri_1$plan_Replaf_2017[is.na(dfri_1$plan_2017_non_imput)]

  summary(dfri_1$plan_2017)

  dfri_1 <- dfri_1 %>% mutate(aide_conc=paste0(AIDEHUM,"_" ,AUTREAIDE,"_" , AIDEPONC,"_",AIDEHEB,"_",AIDEACC,"_" ,AIDETRANS))
  freq(dfri_1$aide_conc,useNA="always",plot=F)


  dfri_1 <- dfri_1 %>% mutate (plan_noti_2017 = case_when(
    !is.na(plan_2017_non_imput) | is.na(AIDEHUM_NOT)
    ~ plan_2017,
    is.na(plan_2017_non_imput) & !is.na(AIDEHUM_NOT) & PLAFOND_2017<AIDEHUM_NOT
    ~ AIDEHUM_NOT,
    is.na(plan_2017_non_imput) & !is.na(AIDEHUM_NOT) & PLAFOND_2017>=AIDEHUM_NOT
    & plan_2017 >=AIDEHUM_NOT
    ~ plan_2017,
    is.na(plan_2017_non_imput) & !is.na(AIDEHUM_NOT) & PLAFOND_2017>=AIDEHUM_NOT
    & plan_2017<AIDEHUM_NOT & aide_conc %in% c("1_0_0_9_9_9","1_0_9_0_0_9","1_0_9_9_9_9","1_9_9_0_0_0")
    ~ AIDEHUM_NOT,
    is.na(plan_2017_non_imput) & !is.na(AIDEHUM_NOT) & PLAFOND_2017>=AIDEHUM_NOT
    & plan_2017<AIDEHUM_NOT &
      !(aide_conc %in% c("1_0_0_9_9_9","1_0_9_0_0_9","1_0_9_9_9_9","1_9_9_0_0_0"))
    ~ runif(nrow(dfri_1),AIDEHUM_NOT,PLAFOND_2017)
  ))


  summary(dfri_1$plan_noti_2017)
  test <- dfri_1 %>% filter(plan_noti_2017!=plan_2017)#3749

  test<-dfri_1 %>% filter(is.na(plan_noti_2017))#0
  test <- dfri_1 %>% filter(plan_noti_2017<AIDEHUM_NOT)#0
  test <- dfri_1 %>% filter(plan_2017<AIDEHUM_NOT)#3749
  test <- dfri_1 %>% filter(plan_noti_2017>PLAFOND_2017)#14433

  #recalcul de proportion_ah_annee_base
  dfri_1 <- dfri_1 %>% mutate(proportion_ah_annee_base= AIDEHUM_NOT/plan_noti_2017)

  summary(dfri_1$proportion_ah_annee_base)

  #pour l'instant on complète les manquantes à 1
  dfri_1 <- dfri_1 %>% mutate(proportion_ah_annee_base=case_when(
    is.na(proportion_ah_annee_base)  ~ 1,
    TRUE~ proportion_ah_annee_base
  ))

  summary(dfri_1$proportion_ah_annee_base)



  dfri_1 <- dfri_1 %>% mutate(retraite_foyer=RESSOURC_APA * 12)
  summary(dfri_1$retraite_foyer)


  #dfri$n_part <- 1 + 1*(dfri$couple_rfs == "1")
  dfri_1 <- dfri_1 %>% mutate(n_part=1 + 1*(couple_rfs == "1"))
  summary(dfri_1$n_part)

  dfri_1$carte_inva <- 0

  dfri_1 <- dfri_1 %>%
    rename(
      ressource_apa =RESSOURC_APA)



  dfri_1 <- dfri_1 %>% mutate(TR_ressource_apa = case_when(
    ressource_apa < 803 ~ "1. moins de 803",
    ressource_apa >= 803 & ressource_apa < 1000 ~ "2.[803-1000)",
    ressource_apa >= 1000 & ressource_apa < 1200 ~ "3.[1000-1200)",
    ressource_apa >= 1200 & ressource_apa < 1400 ~ "4.[1200-1400)",
    ressource_apa >= 1400 & ressource_apa < 1600 ~ "5.[1400-1600)",
    ressource_apa >= 1600 & ressource_apa < 1800 ~ "6.[1600-1800)",
    ressource_apa >= 1800 & ressource_apa < 2000 ~ "7.[1800-2000)",
    ressource_apa >= 2000 & ressource_apa < 2500 ~ "8.[2000-2500)",
    ressource_apa >= 2500 ~ "9.plus de 2500"

  ))
  freq(dfri_1$TR_ressource_apa,w=dfri_1$poids_p_c,useNA="always",plot=F)

  summary(dfri_1$AGER)

  dfri_1 <- dfri_1 %>% mutate(TR_AGER = case_when(
    AGER >= 60 & AGER < 65 ~ "1.60-65 ANS",
    AGER >= 65 & AGER < 70 ~ "2.65-69 ans",
    AGER >= 70 & AGER < 75 ~ "3.70-74 ans",
    AGER >= 75 & AGER < 80 ~ "4.75-79 ans",
    AGER >= 80 & AGER < 85 ~ "5.80-84 ans",
    AGER >= 85 & AGER < 90 ~ "6.85-89 ans",
    AGER >= 90 ~ "7.90 ans +"
  ))

  freq(dfri_1$TR_AGER,w=dfri_1$poids_p_c,useNA="always",plot=F)

  print("Premier calage")
  dfri_1 <- calage_cadrage_dom(dfri_1)

  #sauvegarde
  fwrite(dfri_1, file=paste(chemin_imputation_, "base-ano-data_ri2017_imputations_plan_noti.csv",sep='/'))

  #sous consommation et valeur manquantes-------------------
  # Ouverture de la base
  # dfri_1 <- fread(paste0(chemin_imputation_, "/data_ri2017_imputations_plan_noti.csv"))


  # # summary(dfri_1$plan_noti_2017)
  # # summary(dfri_1$plan_2017_non_imput)
  # # summary(dfri_1$NOTPB) # 12790 NA
  # # summary(dfri_1$NOTHPB) # 15433
  # # summary(dfri_1$AIDEHUM_NOT)
  # # summary(dfri_1$DECHPB_APA)#77 581 NA
  # # summary(dfri_1$DECHUM_APA)#200 695 NA
  #
  #
  # #départements supprimés car trop de NA
  dfri_2 <- dfri_1 %>% filter(!(DEP %in% c("002","013","017","054","059","066","082","091")))
  dfri_2 <- dfri_2 %>% mutate(un=1)
  # #528 349
  #


  dfri_2 <- dfri_2 %>% mutate (TP_emp = case_when(
    !is.na(NOTPB)  ~ NOTPB/plan_noti_2017,
    TRUE ~ NA_real_
  ))

  summary(dfri_2$TP_emp)
  test <- dfri_2 %>% filter(TP_emp>1) #1373
  sum(is.na(test$plan_2017_non_imput)) #951 sont imputés

  dfri_2 <- dfri_2 %>% mutate (TP_emp = case_when(
    TP_emp<=1  ~ TP_emp,
    TRUE ~ NA_real_
  ))
  summary(dfri_2$TP_emp)


  dfri_2 <- dfri_2 %>% mutate (plan_conso_2017 = case_when(
    !is.na(DECHPB_APA) & TP_emp!=9 ~ DECHPB_APA/(1-TP_emp),
    TRUE ~ NA_real_
  ))

  summary(dfri_2$plan_conso_2017)

  #Min. 1st Qu.  Median    Mean 3rd Qu.    Max.    NA's
  #   0.0   172.0   339.2   422.5   575.0  1781.0   27612

  test<- dfri_2 %>% filter(DECHPB_APA<DECHUM_APA)
  test<- test %>% select(DECHPB_APA,DECHUM_APA)
  #1579, pas de diff en fait

  dfri_2 <- dfri_2 %>% mutate (ratio_conso_plan = case_when(
    !is.na(plan_conso_2017)  ~ plan_conso_2017/plan_noti_2017,
    TRUE ~ NA_real_
  ))
  summary(dfri_2$ratio_conso_plan)

  test<- dfri_2 %>% filter(ratio_conso_plan<0.95 & !is.na(ratio_conso_plan))
  dfri_2 <- dfri_2 %>% mutate (ss_conso = case_when(
    !is.na(ratio_conso_plan) & ratio_conso_plan>=0.95 ~ 0,
    !is.na(ratio_conso_plan) & ratio_conso_plan<0.95 ~ 1,
    TRUE ~ NA_real_
  ))

  freq(dfri_2$ss_conso,UseNA="always",plot=F)

  dfri_2 <- dfri_2 %>% mutate (montant_ss_conso = case_when(
    !is.na(plan_conso_2017)  ~ plan_noti_2017-plan_conso_2017,
    TRUE ~ NA_real_
  ))
  summary(dfri_2$montant_ss_conso)

  test <- dfri_2 %>% filter(ss_conso==1)
  summary(test$ratio_conso_plan)
  summary(test$montant_ss_conso)

  #idem mais pour ah
  dfri_2 <- dfri_2 %>% mutate (plan_conso_2017_ah = case_when(
    !is.na(DECHUM_APA) & TP_emp!=9 ~ DECHUM_APA/(1-TP_emp),
    TRUE ~ NA_real_
  ))

  summary(dfri_2$plan_conso_2017_ah)

  dfri_2 <- dfri_2 %>% mutate (ratio_conso_plan_ah = case_when(
    !is.na(plan_conso_2017_ah) & !is.na(AIDEHUM_NOT)  ~ plan_conso_2017_ah/AIDEHUM_NOT,
    TRUE ~ NA_real_
  ))
  summary(dfri_2$ratio_conso_plan_ah)


  dfri_2 <- dfri_2 %>% mutate (ss_conso_ah = case_when(
    !is.na(ratio_conso_plan_ah) & ratio_conso_plan_ah>=0.95 ~ 0,
    !is.na(ratio_conso_plan_ah) & ratio_conso_plan_ah<0.95 ~ 1,
    TRUE ~ NA_real_
  ))
  freq(dfri_2$ss_conso_ah,UseNA="always",plot=F)


  dfri_2 <- dfri_2 %>% mutate (montant_ss_conso_ah = case_when(
    !is.na(plan_conso_2017_ah) &  AIDEHUM_NOT>=plan_conso_2017_ah ~ AIDEHUM_NOT-plan_conso_2017_ah,
    TRUE ~ NA_real_
  ))
  summary(dfri_2$montant_ss_conso_ah)

  dfri_3 <- dfri_2 %>% filter(!(is.na(DECHUM_APA) & !is.na(DECNONHUM_APA) & AIDEHUM==1
                                & !(aide_conc %in% c("1_0_0_0_0_0","1_0_0_9_9_9","1_0_9_0_0_9","1_0_9_9_9_9","1_9_9_0_0_0"))))
  #459 184
  dfri_3 <- dfri_3 %>% filter(!(is.na(DECNONHUM_APA) & !is.na(DECHUM_APA) & AIDEHUM==1 & !(aide_conc %in% c("1_0_0_0_0_0","1_0_0_9_9_9","1_0_9_0_0_9","1_0_9_9_9_9","1_9_9_0_0_0"))))
  #429 246



  dfri_3 <- dfri_3 %>% mutate (sexe_couple = case_when(
    SEXE_C==1 &  couple==0 ~ "homme_seul",
    SEXE_C==2 &  couple==0 ~ "femme_seule",
    SEXE_C==1 &  couple==1 ~ "homme_en_couple",
    SEXE_C==2 &  couple==1 ~ "femme_en_couple",
    TRUE ~ NA_character_
  ))
  table(dfri_3$SEXE_C,dfri_3$couple,useNA="always")
  freq(dfri_3$sexe_couple,useNA="always",plot=F)


  class(dfri_3$gir_imput)


  #recherche des modalités de ref
  freq(dfri_3$sexe_couple,useNA="always",plot=F)
  dfri_3 <- dfri_3 %>% mutate (sexe_couple_mod = case_when(
    SEXE_C==1 &  couple==0 ~ "homme_seul",
    SEXE_C==2 &  couple==0 ~ "1.femme_seule",
    SEXE_C==1 &  couple==1 ~ "homme_en_couple",
    SEXE_C==2 &  couple==1 ~ "femme_en_couple",
    TRUE ~ NA_character_
  ))

  f_s<-dfri_3 %>% filter(sexe_couple_mod=="1.femme_seule")
  freq(f_s$TR_AGER,useNA="always",plot=F)
  dfri_3 <- dfri_3 %>% mutate(TR_AGER_mod = case_when(
    AGER >= 60 & AGER < 65 ~ "1.60-65 ANS",
    AGER >= 65 & AGER < 70 ~ "2.65-69 ans",
    AGER >= 70 & AGER < 75 ~ "3.70-74 ans",
    AGER >= 75 & AGER < 80 ~ "4.75-79 ans",
    AGER >= 80 & AGER < 85 ~ "5.80-84 ans",
    AGER >= 85 & AGER < 90 ~ "6.85-89 ans",
    AGER >= 90 ~ "0.90 ans +",
  ))
  f_s<-dfri_3 %>% filter(sexe_couple_mod=="1.femme_seule" & TR_AGER_mod=="0.90 ans +")
  freq(f_s$gir_imput,useNA="always",plot=F)

  dfri_3$gir_imput<-as.character(dfri_3$gir_imput)
  dfri_3 <- dfri_3 %>% mutate(gir_imput_mod = case_when(
    gir_imput=="1" ~ "gir 1",
    gir_imput=="2" ~ "gir 2",
    gir_imput=="3" ~ "gir 3",
    gir_imput=="4" ~ "0.gir 4",

  ))
  f_s<-dfri_3 %>% filter(sexe_couple_mod=="1.femme_seule" & TR_AGER_mod=="0.90 ans +" & gir_imput_mod=="0.gir 4")
  freq(f_s$TR_ressource_apa,useNA="always",plot=F)
  dfri_3 <- dfri_3 %>% mutate(TR_ressource_apa_mod = case_when(
    ressource_apa < 803 ~ "1. moins de 803",
    ressource_apa >= 803 & ressource_apa < 1000 ~ "2.[803-1000)",
    ressource_apa >= 1000 & ressource_apa < 1200 ~ "3.[1000-1200)",
    ressource_apa >= 1200 & ressource_apa < 1400 ~ "0.[1200-1400)",
    ressource_apa >= 1400 & ressource_apa < 1600 ~ "5.[1400-1600)",
    ressource_apa >= 1600 & ressource_apa < 1800 ~ "6.[1600-1800)",
    ressource_apa >= 1800 & ressource_apa < 2000 ~ "7.[1800-2000)",
    ressource_apa >= 2000 & ressource_apa < 2500 ~ "8.[2000-2500)",
    ressource_apa >= 2500 ~ "9.plus de 2500"

  ))
  f_s<-dfri_3 %>% filter(sexe_couple_mod=="1.femme_seule" & TR_AGER_mod=="0.90 ans +" & gir_imput_mod=="0.gir 4" & TR_ressource_apa_mod=="0.[1200-1400)" )
  freq(f_s$DEP,useNA="always",plot=F)

  class(dfri_3$DEP)
  dfri_3 <- dfri_3 %>% mutate(DEP_mod  = case_when(DEP == "062" ~ ".062",
                                                   TRUE ~ DEP))
  freq(dfri_3$DEP_mod,useNA="always",plot=F)

  summary(dfri_3$ratio_conso_plan)
  set.seed(234)
  model <- tobit(ratio_conso_plan ~
                   TR_AGER_mod + sexe_couple_mod+ TR_ressource_apa_mod  +plan_noti_2017
                 +gir_imput_mod +DEP_mod

                 ,
                 dist ="gaussian",
                 left= 0,
                 right = 1,
                 data = dfri_3,
                 na.action = "na.exclude")

  summary(model)
  fitt <- predict(model, newdata = dfri_3 )
  fitt <- data.frame("ID" = dfri_3$ID,
                     "yhat" = fitt)



  # On ajoute le besoin d'aide imputé à partir de la prédiction Tobit
  # pour cela on left_join la table apa_domicile_3 avec le fitted par la clef ID
  dfri_3 <- dfri_3 %>% left_join(fitt, by = "ID")
  dfri_3 <- dfri_3 %>% rename(ratio_conso_plan_imput = yhat)

  summary(dfri_3$ratio_conso_plan)
  summary(dfri_3$ratio_conso_plan_imput)
  test<- dfri_3 %>% filter(ratio_conso_plan_imput>1)
  #91 661 obs
  sum(is.na(test$plan_conso_2017))
  #3341

  dfri_3$ratio_conso_plan_imput[dfri_3$ratio_conso_plan_imput>1] <-1

  dfri_3 <- dfri_3 %>%
    mutate(plan_conso_2017_imput =
             ratio_conso_plan_imput * plan_noti_2017)

  dfri_3 <- dfri_3 %>% mutate(plan_conso_2017_non_imput=plan_conso_2017)
  dfri_3$plan_conso_2017[is.na(dfri_3$plan_conso_2017_non_imput)] <- dfri_3$plan_conso_2017_imput[is.na(dfri_3$plan_conso_2017_non_imput)]
  summary(dfri_3$plan_conso_2017_non_imput)
  summary(dfri_3$plan_conso_2017)

  dfri_3 <- calage_cadrage_dom(dfri_3)

  fwrite(dfri_3, file=paste(chemin_imputation_, "base-ano-data_ri2017_imputations_plan_conso.csv",sep='/'))

  return("Imputations terminées")
}
