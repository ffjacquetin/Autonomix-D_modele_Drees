# Copyright (C) 2021. Logiciel élaboré par l'État, via la Drees.

# Pole Simulation du bureau Handicap et Dépendance, Drees.

# Ce programme informatique a été développé par la Drees. Il permet d'exécuter le modèle de microsimulation Autonomix version 1 de décembre 2021.

# Ce programme a été exécuté le 10/12/2021 avec la version 4.0.5 de R et les packages mobilisés sont listés dans le fichier DESCRIPTION du projet »

# La documentation du modèle peut être consultése sur gitlab : https://drees_code.gitlab.io/OSOL/bhd/autonomix-documentation-externe/

# Ce programme utilise les données de la base CARE Ménage 2015, ou des RI 2017 ou des RI 2017 anonymisées dans leur version du <date de la version utilisée>.

# Bien qu'il n'existe aucune obligation légale à ce sujet, les utilisateurs de ce programme sont invités à signaler à la DREES leurs travaux issus de la réutilisation de ce code, ainsi que les éventuels problèmes ou anomalies qu'ils y rencontreraient, en écrivant à DREES-CODE@sante.gouv.fr

# Ce logiciel est régi par la licence EUPL V1.2 soumise au droit français et européen, respectant les principes de diffusion des logiciels libres. Vous pouvez utiliser, modifier et/ou redistribuer ce programme sous les conditions de la licence EUPL V1.2, telle que diffusée sur https://eupl.eu/1.2/fr/ et reproduite dans le fichier LICENCE diffusé conjointement au présent programme.
# En contrepartie de l'accessibilité au code source et des droits de copie, de modification et de redistribution accordés par cette licence, il n'est offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons, seule une responsabilité restreinte pèse sur l'auteur du programme, le titulaire des droits patrimoniaux et les concédants successifs.
# À cet égard l'attention de l'utilisateur est attirée sur les risques associés au chargement, à l'utilisation, à la modification et/ou au développement et à la reproduction du logiciel par l'utilisateur étant donné sa spécificité de logiciel libre, qui peut le rendre complexe à manipuler et qui le réserve donc à des développeurs et des professionnels avertis possédant des connaissances informatiques approfondies. Les utilisateurs sont donc invités à charger et tester l'adéquation du logiciel à leurs besoins dans des conditions permettant d'assurer la sécurité de leurs systèmes et ou de leurs données et, plus généralement, à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.


evolution_inflation <- function(dt, taux_inflation_,
                                mode_repartition_ = "prorata", # "priorite aide humaine"
                                mode_besoin_ = "montant 2017",  # "deplafonne",
                                annee_simulation_,
                                tarif_plancher_presta_
                                ){

  #' evolution_inflation
  #'
  #' Calcule le montant total du plan en année de simulation et les montants en aide humaine et en autre aide.
  #' Les autres aides évoluent avec l'inflation, l'aide humaine avec les tarifs horaires
  #'
  #'
  #' @param dt data.table, données à analyser
  #' @param taux_inflation_ numeric, taux d'inflation entre 2017 et l'année de simulation
  #' @param mode_repartition_ string, mode de répartition du montant replafonné entre les aides humaines et les autres aides. "prorata" : calcul au prorata du montant vieilli total couvert par le plafond. "priorite aide humaine" : on réduit dans un premier temps le montant des autres aides, si celui ci dépasse le plafond, on commence à réduire l'aide humaine
  #' @param mode_besoin_ string, montant du besoin d'aide 2017 utilisé : "montant 2017" est le vrai montant du plan d'aide, "deplafonné" est une estimation du montant du besoin d'aide en 2017
  #'
  #' @export
  #'
  #' @importFrom data.table :=

  if (mode_besoin_ == "montant 2017") {

    dt[, montant_vieilli_autreaide_annee_sim := (plan_noti_2017 - AIDEHUM_NOT) * taux_inflation_]

    dt[, montant_vieilli_aidehum_annee_sim := pmax(HEURES_PREST_COR, 0, na.rm=TRUE)*pmax(tarif_presta_annee_sim, 0,  na.rm=TRUE) +
         pmax(HEURES_MAND_COR, 0, na.rm=TRUE)*pmax(tarif_mand_annee_sim, 0,  na.rm=TRUE) +
         pmax(HEURES_GRE_COR, 0, na.rm=TRUE)*pmax(tarif_gre_annee_sim, 0,  na.rm=TRUE)]

  }

  if (mode_besoin_ == "deplafonne") {

    # à re-comprendre

#    dt[, pred_besoin_autreaide_desat_2017 := pred_besoin_AUTREAIDE_NOT0 * pred_AUTREAIDE]
#    dt[, montant_vieilli_aide_annee_sim := pred_besoin_aide * taux_inflation_]
#    dt[, montant_vieilli_autreaide_annee_sim := pred_besoin_AUTREAIDE_NOT0 * taux_inflation_ * pred_AUTREAIDE] # dans l'hyp de désaturation
#    dt[, montant_vieilli_aidehum_annee_sim := montant_vieilli_aide_annee_sim -  montant_vieilli_autreaide_annee_sim] # avec les "vrais" besoins
#    dt[, pred_besoin_autreaide_desat_2017 := NULL]

    # à la place on deplafonne le plan total avec la fonction deplafonnement
    # et on applique des ratio, les mêmes que la partie précédente

    dt[, ratio_autreaide := AIDEHUM_NOT / plan_noti_2017 ]

    dt[, montant_vieilli_aide_annee_sim := pred_besoin_aide * taux_inflation_]

    dt[,  montant_vieilli_aidehum_annee_sim := ratio_autreaide * montant_vieilli_aide_annee_sim ]

    dt[,  montant_vieilli_autreaide_annee_sim := montant_vieilli_aide_annee_sim - montant_vieilli_aidehum_annee_sim ]


  }

  if(!mode_besoin_ %in% c("montant 2017","deplafonne")) {
    print("Le mode besoin est incorrect")
  }



  # tarif plancher heures prestataires à 22 euros
  # augmente les plans concernés
  #  partir de 2022
  # TODO : mettre en paramètres le tarif

  # calcul du nombre d'heures à partir du tarif vieilli (sans tarif plancher)

  dt[type_inter =="presta", nb_heures_presta_annee_sim := montant_vieilli_aidehum_annee_sim / tarif_presta_annee_sim ]
  dt[type_inter =="mand", nb_heures_mand_annee_sim := montant_vieilli_aidehum_annee_sim / tarif_mand_annee_sim ]
  dt[type_inter =="gre a gre", nb_heures_gre_annee_sim := montant_vieilli_aidehum_annee_sim / tarif_gre_annee_sim ]

  dt[type_inter == "presta et gre", `:=`(nb_heures_presta_annee_sim = montant_vieilli_aidehum_annee_sim / (tarif_presta_annee_sim + tarif_gre_annee_sim),
                                          nb_heures_gre_annee_sim = montant_vieilli_aidehum_annee_sim / (tarif_presta_annee_sim + tarif_gre_annee_sim))]

  dt[type_inter == "presta et mand", `:=`(nb_heures_presta_annee_sim = montant_vieilli_aidehum_annee_sim / (tarif_presta_annee_sim + tarif_mand_annee_sim),
                                           nb_heures_mand_annee_sim = montant_vieilli_aidehum_annee_sim / (tarif_presta_annee_sim + tarif_mand_annee_sim))]

  dt[type_inter == "mand et gre", `:=`(nb_heures_gre_annee_sim = montant_vieilli_aidehum_annee_sim / (tarif_gre_annee_sim + tarif_mand_annee_sim),
                                        nb_heures_mand_annee_sim = montant_vieilli_aidehum_annee_sim / (tarif_gre_annee_sim + tarif_mand_annee_sim))]

  dt[type_inter == "les 3", `:=`(nb_heures_gre_annee_sim = montant_vieilli_aidehum_annee_sim / (tarif_gre_annee_sim + tarif_mand_annee_sim + tarif_presta_annee_sim),
                                  nb_heures_mand_annee_sim = montant_vieilli_aidehum_annee_sim / (tarif_gre_annee_sim + tarif_mand_annee_sim + tarif_presta_annee_sim),
                                  nb_heures_presta_annee_sim = montant_vieilli_aidehum_annee_sim / (tarif_gre_annee_sim + tarif_mand_annee_sim + tarif_presta_annee_sim))]


  dt[, tarif_presta_avant_tarif_plancher_annee_sim := tarif_presta_annee_sim]
  dt[, nb_heures_presta_annee_sim_init := nb_heures_presta_annee_sim]


  #

  dt[, montant_vieilli_aide_annee_sim := montant_vieilli_autreaide_annee_sim + montant_vieilli_aidehum_annee_sim]

  dt[, montant_vieilli_aide_annee_sim_avant_tarif_plancher_prestataire := montant_vieilli_aide_annee_sim]

  # application plancher 22 euros
  # le tarif est modifié, et le montant d'aide humaine est aussi réhaussé pour revoir le partage aide humaine vs autre

  if(annee_simulation_ >= 2022) {
    dt[AIDEHUM_PRESTA==1, montant_vieilli_aide_annee_sim := montant_vieilli_aide_annee_sim +
          nb_heures_presta_annee_sim*pmax(0, tarif_plancher_presta_ - tarif_presta_annee_sim)]
    dt[AIDEHUM_PRESTA==1, montant_vieilli_aidehum_annee_sim := montant_vieilli_aidehum_annee_sim +
         nb_heures_presta_annee_sim*pmax(0, tarif_plancher_presta_ - tarif_presta_annee_sim)]
    dt[, tarif_presta_annee_sim := pmax(tarif_plancher_presta_, tarif_presta_annee_sim)]
  }

  # plafonnement

  dt[, montant_plan_aide_annee_sim := pmin(montant_vieilli_aide_annee_sim, plaf_gir_annee_sim)]
  dt[, montant_plan_aide_annee_sim_avant_tarif_plancher_prestataire := pmin(montant_vieilli_aide_annee_sim_avant_tarif_plancher_prestataire, plaf_gir_annee_sim)]



  # en cas de hausse des plafonds, on ne revalorise pas tout car les plans de sont pas révisés systématiquement (tous les 3, 4 ans ?)
  # on applique donc les plafons de N-1 sur la moitié des plans (au hasard) pour limiter la hausse de ces plans
  # TODO : prendre directement le plan calculé (montant vieilli + plafonds) de N-1

#  dt[, montant_plan_aide_annee_sim_moins1 := pmin(pmin(montant_vieilli_aide_annee_sim, plaf_gir_annee_sim_moins1), plaf_gir_annee_sim)]

# set.seed(1)
#  dt[, unif :=round(runif(.N,0,1))]
#  dt[, ind_hasard := case_when(unif < 0.5 ~ 1,TRUE ~ 0)]
#  dt[, montant_plan_aide_annee_sim := (ind_hasard==1) * montant_plan_aide_annee_sim_moins1 + (ind_hasard==0) * montant_plan_aide_annee_sim ]


  dt[, taux_couverture_besoin_annee_sim := montant_plan_aide_annee_sim/montant_vieilli_aide_annee_sim]


  if(mode_repartition_ == "prorata"){

    dt[, montant_autreaide_annee_sim := montant_vieilli_autreaide_annee_sim*taux_couverture_besoin_annee_sim]
    dt[, montant_aidehum_annee_sim := montant_vieilli_aidehum_annee_sim*taux_couverture_besoin_annee_sim]

  }
  # on plafonne les deux volets au prorata du besoin couvert par le plan

  if(mode_repartition_ == "priorite aide humaine"){

    dt[, montant_aidehum_annee_sim := pmin(montant_vieilli_aidehum_annee_sim, montant_plan_aide_annee_sim)]
    dt[, montant_autreaide_annee_sim := montant_plan_aide_annee_sim - montant_aidehum_annee_sim]

  }


  return(dt)
}

evolution_tarifs_ah <- function(dt, annee_simulation_, taux_evol_presta_ = 0.013,
                                taux_evol_mand_= 0.01, taux_evol_gre_= 0.01){

  #' evolution_tarifs_ah
  #'
  #' Calcul du tarif horaire des heures d'aide humaine pour l'année de simulation.
  #'
  #' @param dt data.table, données à analyser
  #' @param annee_simulation_ numeric, année de simulation
  #' @param  taux_evol_presta_ numeric, taux d'évolution annuel des tarifs prestataires à appliquer. Defaut : évolution moyenne annuelle du tarif horaire médian entre 2011 et 2017
  #' @param  taux_evol_mand_ numeric, taux d'évolution annuel des tarifs mandataires à appliquer. Defaut : évolution moyenne annuelle du tarif horaire médian entre 2011 et 2017
  #' @param  taux_evol_gre_ numeric, taux d'évolution annuel des tarifs gré à gré à appliquer. Defaut : évolution moyenne annuelle du tarif horaire médian entre 2011 et 2017
  #'
  #' @export
  #'
  #' @importFrom data.table :=


  dt[, tarif_presta_annee_sim := TARIF_PRESTA_COR*(1+taux_evol_presta_)^(annee_simulation_ - 2017)]
  dt[, tarif_mand_annee_sim := TARIF_MAND_COR*(1+taux_evol_mand_)^(annee_simulation_ - 2017)]
  dt[, tarif_gre_annee_sim := TARIF_GRE_COR*(1+taux_evol_gre_)^(annee_simulation_ - 2017)]

  print(paste0("Multiplicateur des tarifs prestataire appliqué ", (1+taux_evol_presta_)^(annee_simulation_ - 2017)))


  return(dt)
}

nb_heures_par_type_emploi <- function(dt){

  #' nb_heures_par_type_emploi
  #'
  #' Calcul du nombre d'heures en année de simulation par mode d'emploi à partir du montant en aide humaine en année de simulation et des tarifs horaires.
  #' Hypothèse : quand un bénéficiaire emploie plusieurs types de service, il emploie le même nombre d'heures dans chaque service.
  #'
  #' @param dt data.table, données à analyser
  #' @export
  #'
  #' @importFrom data.table :=


  dt[type_inter =="presta", nb_heures_presta_annee_sim := montant_aidehum_annee_sim / tarif_presta_annee_sim ]
  dt[type_inter =="mand", nb_heures_mand_annee_sim := montant_aidehum_annee_sim / tarif_mand_annee_sim ]
  dt[type_inter =="gre a gre", nb_heures_gre_annee_sim := montant_aidehum_annee_sim / tarif_gre_annee_sim ]

  dt[type_inter == "presta et gre", `:=`(nb_heures_presta_annee_sim = montant_aidehum_annee_sim / (tarif_presta_annee_sim + tarif_gre_annee_sim),
                                         nb_heures_gre_annee_sim = montant_aidehum_annee_sim / (tarif_presta_annee_sim + tarif_gre_annee_sim))]

  dt[type_inter == "presta et mand", `:=`(nb_heures_presta_annee_sim = montant_aidehum_annee_sim / (tarif_presta_annee_sim + tarif_mand_annee_sim),
                                          nb_heures_mand_annee_sim = montant_aidehum_annee_sim / (tarif_presta_annee_sim + tarif_mand_annee_sim))]

  dt[type_inter == "mand et gre", `:=`(nb_heures_gre_annee_sim = montant_aidehum_annee_sim / (tarif_gre_annee_sim + tarif_mand_annee_sim),
                                       nb_heures_mand_annee_sim = montant_aidehum_annee_sim / (tarif_gre_annee_sim + tarif_mand_annee_sim))]

  dt[type_inter == "les 3", `:=`(nb_heures_gre_annee_sim = montant_aidehum_annee_sim / (tarif_gre_annee_sim + tarif_mand_annee_sim + tarif_presta_annee_sim),
                                 nb_heures_mand_annee_sim = montant_aidehum_annee_sim / (tarif_gre_annee_sim + tarif_mand_annee_sim + tarif_presta_annee_sim),
                                 nb_heures_presta_annee_sim = montant_aidehum_annee_sim / (tarif_gre_annee_sim + tarif_mand_annee_sim + tarif_presta_annee_sim))]

  return(dt)

}



# TODO : tarif 22e sera dans la V2 du package



calcul_plan_tarif_plancher <- function(dt_, montant_tarif_plancher_) {

  #' calcul_plan_tarif_plancher
  #'
  #' Calcul de la répercussion du tarif plancher des heures prestataires au plan d'aide en année de simulation.
  #'
  #' @param dt data.table, données à analyser
  #' @param montant_tarif_plancher_ numeric, nouveau tarif horaire d'une heure prestataire après application du tarif plancher
  #' @export
  #'
  #' @importFrom data.table :=

  dt_[, tarif_presta_apres_tarif_plancher_annee_sim := pmax(montant_tarif_plancher_, tarif_presta_annee_sim)]
  dt_[, plan_apres_tarif_plancher_annee_sim := montant_plan_aide_annee_sim]
  dt_[AIDEHUM_PRESTA==1,
      plan_apres_tarif_plancher_annee_sim := montant_plan_aide_annee_sim +
        nb_heures_presta_annee_sim*pmax(0, montant_tarif_plancher_ - tarif_presta_annee_sim)]

  return(dt_)

}


nveau_plafond <- function(dt_, gir_, plaf_gir_) {

  #' nveau_plafond
  #' Pour un gir donné et un montant de plafond donné, calcule le taux de saturation après application du tarif plancher
  #' On cherche les nouveaux plafonds par GIR pour avoir les taux de saturation avant augmentation.
  #' On raisonne volontairement sans l'effet indirect de l'augmentation des plafonds pour les plans sans mode d'emploi prestataire
  #'
  #' @param dt_ data.table, données à analyser
  #' @param gir_ numeric, gir considéré
  #' @param plaf_gir_ numeric, montant du plafond de départ
  #' @export
  #'
  #' @importFrom data.table :=

  dfg <- dt_[gir_imput == gir_]
  res_sat <- weighted.mean(1*(dfg$plan_apres_tarif_plancher_annee_sim >= plaf_gir_),
                           w=dfg$poids_post_calage, na.rm=TRUE)*100
  return(res_sat)
}


gen_datagir <- function(dt_, gir_, plaf_min_, seuil_=0.96) {

  #' gen_datagir
  #' Génére un data.table contenant des montants de plafond et le taux de saturation des plans associé
  #' après application de la mesure tarif plancher pour les heures d'aide humaine prestataire.
  #' La saturation est à 96% du plafond
  #'
  #' @param dt_ data.table, données à analyser
  #' @param gir_ numeric, gir considéré
  #' @param plaf_min_ numeric, montant du plafond de départ
  #' @export
  #'
  #' @importFrom data.table :=


  plaf96 <- seq(plaf_min_, by=1, length.out = 100)*seuil_

  nvelle_sat_gir <- sapply(plaf96, function(x) nveau_plafond(dt_, gir_, x))

  dtres <- data.table(plafond = plaf96/seuil_, taux_saturation96 = nvelle_sat_gir)

  return(dtres)
}


determination_nv_plafond <- function(dt_, gir_, seuil_=0.96) {

  #' determination_nv_plafond
  #'
  #' Determine le nouveau plafond
  #'
  #'
  #' @param dt data.table, données à analyser
  #' @param gir_ numeric, gir considéré
  #' @export
  #'
  #' @importFrom data.table :=

  df1 <- gen_datagir(dt_, gir_, max(dt_[gir_imput == gir_]$plaf_gir_annee_sim), seuil_=seuil_)

  taux_sat_initial <- sum(dt_[gir_imput == gir_ &
                                montant_plan_aide_annee_sim >= seuil_*plaf_gir_annee_sim]$poids_post_calage) /
    sum(dt_[gir_imput == gir_]$poids_post_calage)*100

  print(df1)
  print("Taux de saturation initial, avant mesure tarif minimum : ")
  print(taux_sat_initial)

  res <- df1[which.min(abs(df1$taux_saturation96 - taux_sat_initial))]$plafond

  return(res)

}

estimation_compo_22e <- function(dt, nveau_tarif=23, plaf_gir1 = 1774, plaf_gir2 = 1428,
                                 plaf_gir3 = 1034, plaf_gir4 = 691) {


    parametres$mtp_annee_sim <- 1126.41
    parametres$coef_plaf_gir1_asv_annee_sim <- 1.605
    parametres$coef_plaf_gir2_asv_annee_sim <- 1.298
    parametres$coef_plaf_gir3_asv_annee_sim <- 0.938
    parametres$coef_plaf_gir4_asv_annee_sim <- 0.626


  #' estimation_compo_22e
  #'
  #' Calcul des plans notifiés en année de simulation avec les nouveaux plafonds post tarif plancher
  #'
  #' @param dt data.table
  #' @param nveau_tarif numeric, nouveau montant minimal d'une heure prestataire
  #' @param plaf_gir1 numeric, nouveau plafond pour les GIR 1
  #' @param plaf_gir2 numeric, nouveau plafond pour les GIR 2
  #' @param plaf_gir3 numeric, nouveau plafond pour les GIR 3
  #' @param plaf_gir4 numeric, nouveau plafond pour les GIR 4
  #'
  #' @export
  #'
  #' @importFrom data.table :=

  # Nouveaux plafonds
  dt[gir_imput == 1, nveau_plafond_annee_sim := plaf_gir1]
  dt[gir_imput == 2, nveau_plafond_annee_sim := plaf_gir2]
  dt[gir_imput == 3, nveau_plafond_annee_sim := plaf_gir3]
  dt[gir_imput == 4, nveau_plafond_annee_sim := plaf_gir4]

  # "plan_sans_plafond_apres_evolution_tarif_annee_sim" : avant replafonnement, montant du plan en année de simulation avec
  # tarifs planchers pour les bénéficiaires d'aide humaine prestataire et pour les autres, le montant vieilli de leur plan 2017
#  dt[, plan_sans_plafond_apres_evolution_tarif_annee_sim :=
#       pmax(plan_apres_tarif_plancher_annee_sim, montant_vieilli_aide_annee_sim)] # version naive, le calcul pourra être affiné

    dt[, plan_sans_plafond_apres_evolution_tarif_annee_sim :=
         pmax(plan_apres_tarif_plancher_annee_sim, montant_plan_aide_annee_sim)] # version naive, le calcul pourra être affiné

  dt[, plan_avec_plafond_apres_evolution_tarif_annee_sim := pmin(plan_sans_plafond_apres_evolution_tarif_annee_sim, nveau_plafond_annee_sim)]

  # # suppression des colonnes "vieillies"
  # dt[, montant_vieilli_autreaide_annee_sim := NULL]
  # dt[, montant_vieilli_aidehum_annee_sim := NULL]
  # dt[, montant_vieilli_aide_annee_sim := NULL]


  # Calcul du nombre d'heures prestataires qui ne peuvent plus être notifiées après le tarif plancher
  # en faisant l'hypothèse que ce sont les premières à être enlevées du plan (pourrait être affiné)
  # uniquement sur les plans qui ont de l'aide prestataire
  dt[, nb_heures_presta_manquantes_annee_sim := 0]
  dt[AIDEHUM_PRESTA==1,
     nb_heures_presta_manquantes_annee_sim :=
       (plan_sans_plafond_apres_evolution_tarif_annee_sim - plan_avec_plafond_apres_evolution_tarif_annee_sim) /
       tarif_presta_apres_tarif_plancher_annee_sim]

  # Calculer de l'APA en passant au plan consommé
  dt[, plan_consomme_avec_plafond_apres_evolution_tarif_annee_sim :=
       plan_avec_plafond_apres_evolution_tarif_annee_sim * plan_conso_2017/plan_noti_2017]

  dt[, particip_apres_tarif_plancher_annee_sim :=
       partic_finan_avec_ASV(ressource_apa_annee_precedente_sim, plan_avec_plafond_apres_evolution_tarif_annee_sim, parametres)]

  dt[, taux_participation_apres_tarif_plancher_annee_sim := particip_apres_tarif_plancher_annee_sim / plan_avec_plafond_apres_evolution_tarif_annee_sim ]

  dt[, apa_apres_tarif_plancher_annee_sim := (1 -taux_participation_apres_tarif_plancher_annee_sim) * plan_consomme_avec_plafond_apres_evolution_tarif_annee_sim]

  dt[,depense_apres_apa_apres_tarif_plancher:=plan_consomme_avec_plafond_apres_evolution_tarif_annee_sim - apa_apres_tarif_plancher_annee_sim]

  # Crédit d'impôt pour emploi d'un salarié à domicile ####
  dt[,
      credit_impot_apres_tarif_annee_sim :=
        calcul_credit_impot_salarie_dom(
          depense_apres_apa_apres_tarif_plancher,
          couple_rfs,
          carte_inva,
          AGE_C,
          parametres,
          taux = 0.5
        )]
  dt[, irpp_final_apres_tarif_annee_sim :=irpp_apres_decote_et_20_annee_sim - credit_impot_apres_tarif_annee_sim]
  dt[, credit_impot_apres_tarif_annee_sim_mens := credit_impot_apres_tarif_annee_sim /12]
  dt[, RAC_apres_tarif_annee_sim := depense_apres_apa_apres_tarif_plancher - credit_impot_apres_tarif_annee_sim_mens]
  dt[, taux_effort_apres_tarif_annee_sim := (RAC_apres_tarif_annee_sim / ress_tot_annee_sim)*100]


  return(dt)
}



