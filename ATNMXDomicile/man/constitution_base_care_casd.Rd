% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/1CARE_Autonomix-module_constitution_base_CASD.R
\name{constitution_base_care_casd}
\alias{constitution_base_care_casd}
\title{constitution_base_care_casd}
\usage{
constitution_base_care_casd(chemin_bases_, chemin_const_)
}
\arguments{
\item{chemin_bases_}{character, chemin vers les bases brutes}

\item{chemin_const_}{character, chemin où écrire les bases créées par cette fonction}
}
\value{
data.frame contenant les données care consolidées
}
\description{
constitution_base_care_casd
}
