---
title: "README_Domicile_General"
author: "Stéphanie Boneschi"
date: "09/11/2021"
output: html_document
---


# Introduction
Le volet Domicile permet d’appréhender les dispositifs de prise en charge à domicile de la perte d’autonomie en France : allocation personnalisée d’autonomie, déductions fiscales,… Il permet de produire des indicateurs statistiques tel que le reste à charge lié à la perte d’autonomie et les taux d'effort, et de réaliser des chiffrages de coût et d’impacts redistributifs des mesures règlementaires existantes ou à venir. <br>
Le champ du volet Domicile est celui des personnes agées de 60 ans ou plus, dépendantes bénéficiaires de l’APA à domicile en France métropolitaine et DROM (hors Mayotte).

# Données

Les données mobilisées peuvent être celles de l’enquête CARE-ménages de 2015 ou celles des remontées individuelles sur l’APA des conseils départementaux de 2017, selon la finalité envisagée. 

##  CARE Ménages
L’enquête CARE-Ménages, s’est déroulée de mai à octobre 2015. Elle a trois principaux objectifs : suivre l’évolution de la dépendance, estimer le reste à charge lié à la dépendance et mesurer l’implication de l’entourage auprès de la personne âgée. Le volet « seniors » de l’enquête CARE-ménages s’intéresse aux conditions de vie des personnes âgées, à leurs difficultés à réaliser les activités de la vie quotidienne et aux aides qu’elles reçoivent. <br>
Les données de l’enquête CARE-Ménages ont été enrichies par les données des Conseils Départementaux (CD) et par les données fiscales et sociales (RFS) 2014. Sur les 10 628 seniors de l’enquête, seuls les bénéficiaires de l’APA évalués en GIR 1 à 4 ont été conservés. Il s’agit des personnes ayant des droits ouverts à l’APA à domicile en mai 2015, en octobre 2015 ou à la date de l’extraction des données des Conseils Départementaux. La base de travail comprend ainsi 2000 observations.

##  Remontées individuelles 2017
L’opération de remontées individuelles de données sur l’allocation personnalisée d’autonomie (APA) et l’aide sociale à l’hébergement (ASH), dite « RI-APA-ASH 2017 » est une collecte d'informations administratives sur l’ensemble des bénéficiaires de l’APA (à domicile et en établissement) ou de l’ASH en France, au cours de tout ou partie de l’année 2017, ainsi que sur les demandeurs en 2017.  <br>
Les informations recueillies portent sur les caractéristiques sociodémographiques des bénéficiaires, leur niveau de dépendance détaillé ainsi que l’historique des évolutions du niveau de dépendance depuis la première demande d’APA, leurs ressources, les montants de leurs plans d’aide et le contenu de ces plans. 
La base des remontées individuelles pour le volet domicile comprend 575 000 observations et concernent près de 770 000 bénéficiaires de l’APA à domicile. 


## Modèle de projection Livia et enquête annuelle aide sociale
Toutes les années entre le millésime de l’année de référence des données et 2050 peuvent être simulées. 
Le modèle est calé sur les estimations du modèle de projection LIVIA. Pour les années de 2016 à 2019, on réajuste les effectifs projetés par le modèle Livia à partir des effectifs des bénéficiaires de l'APA issus de l'enquête annuelle Aide Sociale. Le tout constitue un ensemble d’observations individuelles cohérentes et représentatives de la population des personnes âgées dépendantes, sur lequel il est possible de se baser afin d’analyser les aides publiques destinées à ces personnes ou d’évaluer ex ante les effets de réformes de ces aides.

 

# Possibilités

Scénario LIVIA


# Variables d'intérêt simulées  
Les éléments simulés sont :<br>
Le montant d’APA versé par le conseil départemental  <br>
Le montant de la participation au plan notifié ou consommé du senior   <br>
L’impôt sur le revenu  <br>
Le crédit d'impôt pour l’emploi d’un salarié à domicile  <br>
La participation au plan après APA et crédit d’impôt du senior  <br>
Le taux d’effort, défini comme le rapport entre la participation du senior au plan notifié ou consommé après APA et crédit  <br> d’impôt, et les ressources mensuelles du senior.


# Hypothèses
Plusieurs hypothèses sont faites sur le non recours : il n’y a pas de non recours au minimum vieillesse, tous les seniors éligibles au crédit d’impôt en font bien la demande dans leur déclaration d’impôt sur le revenu. 
Par simplification, le calcul est réalisé sous l’hypothèse que le plan d’aide finance uniquement de l’emploi d’aide à domicile.
