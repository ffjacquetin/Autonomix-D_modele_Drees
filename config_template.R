rm(list = ls())
library(ATNMXDomicile)

# CHEMINS D'ENREGISTREMENT DES FICHIERS

chemin_donnees_brutes <- 'T:/BHD/BHD - Autonomix/Domicile/Bases'  # Fichier contenant les bases brutes
chemin_donnees_construites <- 'T:/BHD/BHD - Autonomix/Domicile/Bases'  # Fichier où l'on compte écrire / lire les bases construites (issues de la création de la base, de l'imputation et de la simulation)

# PARAMETRES DE L'IMPUTATION : A LANCER UNE SEULE FOIS
nom_branche <- prompt::git_branch()   # Facultatif : permet de trouver automatiquement le nom de la branche
donnees_base <- "ri" #"care" ou "ri" ou "ri_anonymisees"

construit_base(chemin_donnees_brutes, chemin_donnees_construites, donnees_base)
lancement_imputation(chemin_donnees_construites, donnees_base)

# PARAMETRES DE LA SIMULATION
donnees_base <- "ri" #"care" ou "ri" ou "ri_anonymisees"
annee_simulation <- 2021
type_plan <- "plan_conso_avec_compo"  # plan_conso ou plan_noti ou plan_conso_avec_compo pour la pad. ne pas remplir pour care
calage <- TRUE # vaut TRUE si on cale

mode_dep <- FALSE # est ce qu'on fait une pondération départementale ?
scenario_ouverture_places_ehpad <- 3  # parmi 1, 2, 3 (cf. documentation de Livia)
scenario_esperance_vie <- "central"  # parmi "central", "edv_haute", "edv_basse". Défaut : "central"
hypothese_evolution_dependance <- "intermediaire"  # parmi "intermediaire", "optimiste" et "pessimiste". Défaut : "intermediaire"
mode_effectifs_parametres <- "recales aide sociale"  # parmi "recales aide sociale" ou "livia uniquement" : "livia uniquement" correspond à des effectifs de calages qui viennent entièrement du modèle livia,
#' "recales aide sociale" correspond à l'utilisation des effectifs d'aide sociale pour les années observées
mode_repartition <-"prorata" # "prorata" par defaut, autre option "priorite aide humaine"
mode_besoin <-"montant 2017"  # "montant 2017" par defaut, autre option "deplafonne"

lancement_simulation(
  donnees_base,
  annee_simulation,
  calage,
  type_plan,
  mode_dep,
  scenario_ouverture_places_ehpad,
  scenario_esperance_vie,
  hypothese_evolution_dependance,
  mode_effectifs_parametres,
  mode_repartition,
  mode_besoin
)
