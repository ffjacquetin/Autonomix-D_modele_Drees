
te <- fread("C:/Users/albane.mirondelespin/Documents/Autonomix/data_temp/3-Imputations/synthe/data_ri_imputations_plan_conso_avec_compo.csv")
fwrite(te[sample(100)], file = "C:/Users/albane.mirondelespin/Documents/Autonomix/data_temp/3-Imputations/synthe/data_ri_imputations_plan_conso_avec_compo.csv")


colnames(te)
colselectsimu <- c('ID',
              'SEXE_C',
              'couple',
              'gir_imput',
              'AGE_C',
              'poids_p',
              'AIDEHUM',
              'AIDEHUM_NOT',
              'AUTREAIDE',
              'AUTREAIDE_NOT',
              'ratio',
              'couple_rfs',
              'proportion_ah_annee_base',
              'PLAFOND_2017',
              'ressource_apa',
              'ratio_plan_plafond',
              'ratio_plan_plafond_imput',
              'plan_noti_2017',
              'retraite_foyer',
              'n_part',
              'carte_inva',
              'poids_post_imputation',
              'plan_conso_2017')


colselectcompo <- c('ID',
                   'gir_imput',
                   'poids_p',
                   'AIDEHUM',
                   'AIDEHUM_NOT',
                   'AUTREAIDE',
                   'AUTREAIDE_NOT',
                   'ratio',
                   'AIDEHUM_PRESTA',
                   'taux_particip',
                   'ressource_apa',
                   'plan_noti_2017',
                   'plan_conso_2017',
                   'type_inter',
                   'TARIF_PRESTA',
                   'TARIF_MAND',
                   'HEURES_MAND',
                   'TARIF_GRE',
                   'HEURES_GRE',
                   'HEURES_PREST_COR',
                   'HEURES_MAND_COR',
                   'HEURES_GRE_COR',
                   'TARIF_PRESTA_COR',
                   'TARIF_MAND_COR',
                   'TARIF_GRE_COR',
                   'pred_besoin_aide')

cols <- unique(c(colselectsimu, colselectcompo))
te[, ..cols]

fwrite(te[sample(20), ..cols], file = "C:/Users/albane.mirondelespin/Documents/Autonomix/data_temp/3-Imputations/synthe/data_ri_imputations_plan_conso_avec_compo.csv")
# ok fonctionne


